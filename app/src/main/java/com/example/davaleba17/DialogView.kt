package com.example.davaleba17

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.davaleba17.databinding.FragmentDialogViewBinding

class DialogView : Fragment() {
    private lateinit var binding : FragmentDialogViewBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDialogViewBinding.inflate(layoutInflater, container, false)
        return binding.root

    }

}