package com.example.davaleba17

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.Button
import com.example.davaleba17.databinding.ActivityMainBinding
import com.example.davaleba17.databinding.FragmentDialogViewBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        init()
    }
    private fun init(){
        binding.text.setOnClickListener {
            openDialog()
        }
    }

    private fun openDialog(){
        val dialog = Dialog(this)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        val dialogBinding : FragmentDialogViewBinding
        dialogBinding = FragmentDialogViewBinding.inflate(layoutInflater)
        dialog.setContentView(dialogBinding.root)
        val params = dialog.window!!.attributes
        params.width = WindowManager.LayoutParams.MATCH_PARENT
        params.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialogBinding.btnCancel.setOnClickListener {
            dialog.cancel()
        }
        dialog.show()
    }
}